def provision_jenkins(jenkins)
  jenkins.vm.provision 'shell', inline: <<-SHELL
    echo 'Adding jenkins repo'
    wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo apt-key add -
    echo deb https://pkg.jenkins.io/debian-stable binary/ | sudo tee /etc/apt/sources.list.d/jenkins.list
    echo 'Done\n'

    echo 'Install prereqs'
    sudo apt-get update &> /dev/null
    sudo apt-get install openjdk-8-jre-headless -y

    
    echo 'Install Jenkins'
    sudo apt-get --assume-yes install jenkins
    echo 'Done\n'

    echo 'Enable Jenkins Service'
    sudo systemctl enable jenkins
    sudo systemctl start jenkins || echo 'Trouble starting jenkins'
    echo 'Done\n'

    exit 0
  SHELL
end