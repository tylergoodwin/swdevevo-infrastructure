def provision_with_docker(config)
  config.vm.box = "ubuntu/xenial64"
  config.vm.synced_folder ".", "/vagrant", disabled: true

  config.vm.provision 'shell', inline: <<-SHELL
    echo 'Install prereqs'
    sudo apt update &> /dev/null

    sudo apt-get --assume-yes install \
                 apt-transport-https \
                 ca-certificates \
                 curl \
                 software-properties-common
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    echo 'Done\n'

    sudo add-apt-repository \
       "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
       $(lsb_release -cs) \
       stable"
    echo 'Install docker'
    sudo apt-get update &> /dev/null
    sudo apt-get --assume-yes install -y docker-ce
    echo 'Done\n'

    echo 'Install compose'
    [ -f /usr/local/bin/docker-compose ] && rm /usr/local/bin/docker-compose
    sudo curl -L https://github.com/docker/compose/releases/download/1.22.0/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose &> /dev/null
    sudo chmod +x /usr/local/bin/docker-compose
    echo 'Done\n'

    echo 'Add user to docker group'
    sudo usermod -aG docker $USER
    echo 'Done'

    exit 0
  SHELL
end