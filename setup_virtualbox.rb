def setup_virtualbox(config, memory_override = 1024, cpu_override = 2)
  config.vm.provider "virtualbox" do |v|
    v.memory = memory_override
    v.cpus = cpu_override
  end
end